# JPEGcoPy

JPEGcoPy is a simple python based tool to:
1. Extract the JPEG quantization parameters from JPEG files, including:
	- [Quantization tables](https://en.wikipedia.org/wiki/JPEG#Quantization)
	- [Chroma subsampling](https://en.wikipedia.org/wiki/Chroma_subsampling) parameters
2. Encode an image with specified JPEG quantization parameters

For some background on JPEG encoding, the [wikipedia page](https://en.wikipedia.org/wiki/JPEG) provides a good overview.

This tool was inspired by some of my work in the multimedia forensics area, where many algorithms require conditioning on specific JPEG quantization parameters. However, many JPEG conditioning implementations are based on JPEG "Quality Factor," or QF, for which there is no standardized definition and many encoders use their own quantization parameters. This tool extracts the two key JPEG encoding parameters, and apply them to another image.
 
## Required Packages:
- python>=3
- pillow 
 
## Examples

### Extracting quantization parameters
```
    import jpegcopy
    fpath = './myimage.jpg'
    qtab, sub = jpegcopy.get_jpeg_attrs(fpath)  
    
    print(qtab)
    >> {0: array('B', [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 2, 2, 2, 2, 2, 4, 3, 3, 2, 3, 5, 4, 5, 5, 5, 4, 4, 4, 5, 6, 7, 6, 5, 5, 7, 6, 4, 4, 6, 9, 6, 7, 8, 8, 8, 8, 8, 5, 6, 9, 10, 9, 8, 10, 7, 8, 8, 8]),
        1: array('B', [1, 1, 1, 2, 2, 2, 4, 2, 2, 4, 8, 5, 4, 5, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8])}
	 
    print(sub)
    >> 2
```
qtab is the 64 quanitization values, in zig-zag order, for the luminance and chroma channels respectively.

sub  is the chroma subsampling value, according to https://readthedocs.org/projects/pillow/downloads/pdf/stable/ (page 29): 
- 0: equivalent to 4:4:4
- 1: equivalent to 4:2:2
- 2: equivalent to 4:2:0
 
### Saving an image to disk using specified quantization parameters

```
    fpath = './myimage.jpg'
    fout = './myimage_newcompression.jpg'
    jpegcopy.save_to_disk(fpath, fout, qtab, sub)
```

with `qtab` and `sub` derived from `get_jpeg_attrs`

### Encoding an image in memory using specified quantization parameters
Use this if you want to immediately load the encoded image to memory, skipping the disk write and read
```
    import Image
    im = Image.open(fpath)
    I = jpegcopy.copy_jpeg_attrs(fpath, qtab, sub)
```

### Applying the JPEG parameters from one file to another
```
    f_source = './myimage.jpg' #take the quantization parameters from this image
    f_dest = './myotherimage.jpg' #and encode this image using them
    
    #to save to memory
    I = jpegcopy.copy_jpeg_from_source(imIn,imSrc,to_disk=False)
    
    #to save to disk
    jpegcopy.copy_jpeg_from_source(imIn,imSrc,to_disk=True,fout='./mycompressedimage.jpg')
 
```
