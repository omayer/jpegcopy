#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 15 15:57:30 2020

@author: owen
"""
from PIL import Image, JpegImagePlugin
import io

#TODO: Documentation


def get_jpeg_attrs(im):
    if type(im) is str: #if input is a file path string
        im = Image.open(im) #load the Image
        
    quant_dict = im.quantization #get quanitation tables
    subsampling = JpegImagePlugin.get_sampling(im) #get subsampling parameters
    #Possible subsampling values are 0, 1 and 2 that correspond to 4:4:4, 4:2:2 and 4:1:1 (or 4:2:0?)
    #according to https://pillow.readthedocs.io/en/4.0.x/PIL.html#subsampling
    return quant_dict, subsampling

    
def save_to_disk(im,fout,quant_dict,subsampling):
    if type(im) is str: #if input is a file path string
        im = Image.open(im) #load the Image
    
    #save to jpeg using specified subsampling and quant table params
    im.save(fout, format='JPEG', subsampling=subsampling, qtables=quant_dict)
    
    
def copy_jpeg_attrs(im,quant_dict,subsampling):
     buf = io.BytesIO() #create a buffer object
     
     #write im file to buffer using JPEG, with specified subsampling and qtable
     im.save(buf, format='JPEG', subsampling=subsampling, qtables=quant_dict)
     
     #read in image from buffer
     #im_copied looks like input "im" but has specified jpeg and quantization inputs
     im_copied = Image.open(buf)
     return im_copied
    
    
def copy_jpeg_from_source(imIn,imSrc,to_disk=False,fout=None):
    if type(imIn) is str: #if input is a file path string
        imIn = Image.open(imIn) #load the Image
    if type(imSrc) is str: #if source is a file path string
        imSrc = Image.open(imSrc) #load the Image
    
    quant_dict, subsampling = get_jpeg_attrs(imSrc)
    
    if to_disk:
        save_to_disk(imIn,fout,quant_dict,subsampling)
        
    else:
        im_copied = copy_jpeg_attrs(imIn,quant_dict,subsampling)
        return im_copied
